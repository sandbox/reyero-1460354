<?php
/**
 * @file
 * Malo UI entity classes
 */

/**
 * The class used for profile entities.
 */
class MaloCapture extends Entity {

  /**
   * Data fields
   */
  public $mcid;
  public $uid;
  public $nid;
  public $title;
  public $content;
  public $timestamp;
  public $location;
  public $referrer;
  public $hostname;

  public function __construct($values = array()) {
    parent::__construct($values, 'malo_capture');
  }

  /**
   * Returns the full url() for the profile.
   */
  public function url() {
    $uri = $this->uri();
    return url($uri['path'], $uri);
  }

  /**
   * Returns the drupal path to this profile.
   */
  public function path() {
    $uri = $this->uri();
    return $uri['path'];
  }

  public function defaultUri() {
    return array(
      'path' => 'malo/capture/' . $this->mcid,
    );
  }

  public function buildContent($view_mode = 'full', $langcode = NULL) {
    $content = array();
    //$content['user']['#markup'] = theme('username', array('account' => user_load($this->uid)));

    return entity_get_controller($this->entityType)->buildContent($this, $view_mode, $langcode, $content);
  }

  public function save() {
    if (empty($this->timestamp)) {
      $this->timestamp = REQUEST_TIME;
    }
    parent::save();
  }
}

/**
 * Extend the defaults.
 */
class MaloCaptureMetadataController extends EntityDefaultMetadataController {

  public function entityPropertyInfo() {
    $info = parent::entityPropertyInfo();
    $properties = &$info[$this->type]['properties'];

    $properties['title'] = array(
      'label' => t('Title'),
      'description' => t('The capture title.'),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer malo',
      'schema field' => 'title',
    );

    $properties['owner'] = array(
      'label' => t("Owner"),
      'type' => 'user',
      'description' => t("The user whose data was captured."),
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'setter permission' => 'administer malo',
      'required' => TRUE,
      'schema field' => 'owner',
    );

    $properties['user'] = array(
      'label' => t("User"),
      'type' => 'user',
      'description' => t("The user whose data was captured."),
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'setter permission' => 'administer malo',
      'required' => TRUE,
      'schema field' => 'uid',
    );

    $properties['node'] = array(
      'label' => t("Node"),
      'type' => 'node',
      'description' => t("The node where data was captured."),
      'getter callback' => 'entity_property_getter_method',
      'setter callback' => 'entity_property_setter_method',
      'setter permission' => 'administer malo',
      'required' => TRUE,
      'schema field' => 'nid',
    );

    $properties['timestamp'] = array(
      'label' => t("Date"),
      'type' => 'date',
      'description' => t("The date the data was captured."),
      'setter callback' => 'entity_property_verbatim_set',
      'setter permission' => 'administer malo',
      'schema field' => 'timestamp',
    );

    return $info;
  }
}

/**
* Malo UI controller
*/
class MaloCaptureUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Malo UI.';
    $items[$this->path]['type'] = MENU_LOCAL_TASK;
    return $items;
  }
}