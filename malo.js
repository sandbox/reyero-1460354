// Example functions from http://www.w3schools.com/js/js_cookies.asp
/**
* Set cookie value.
*
* @param c_name
*   Cookie name
* @param value
*   Cookie value
*/
function setCookie(c_name , value, exdays, domain)
{
	var exdate=new Date();
	var c_value=escape(value);
	if (exdays) {
		exdate.setDate(exdate.getDate() + exdays);
		c_value += ((exdays==null) ? "" : ";expires="+exdate.toUTCString());
        }
	// For this to work with Drupal session cookies.
	c_value += ';path=/';
	if (domain) {
	  c_value += ';domain=' + domain;
	}
	document.cookie = c_name + "=" + c_value;
	// Warning
	alert("Setting cookie " + c_name + "=" + c_value);
}

/**
 * Get cookie value
 *
 * @param c_name
 *   Cookie name
 */
function getCookie(c_name)
{
	var i,x,y,ARRcookies=document.cookie.split(";");
	for (i=0;i<ARRcookies.length;i++)
	{
	  x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
	  y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
	  x=x.replace(/^\s+|\s+$/g,"");
	  if (x==c_name) {
	    return unescape(y);
	  }
  }
}

/**
 * Show all cookies in a message box.
 */
function showCookies() {
	var text = '';
	var i,x,y,cookies = document.cookie.split(";");
	for (i=0;i < cookies.length;i++)
	{
		text += cookies[i] + '\n';
	}
	alert(text);
}

/**
 * Capture all cookies..
 *
 * @param node_id
 *   Drupal node id to link the cookies to.
 */
function captureCookies(node_id) {
  captureData(node_id, 'Captured cookies js', document.cookie);
}

/**
 * Capture all cookies..
 *
 * @param node_id
 *   Drupal node id to link the cookies to.
 */
function captureData(node_id, title_value, content_value) {
  var path = '/node/' + node_id;
  var params = {title:title_value, content:content_value};
  var capture = buildUrl(path, params);
  new Image().src = capture;
  // Warning
  alert("Data capture: " + capture);
}

/**
 * Redirects to Drupal path
 *
 * @param path
 * 	 Drupal path. Example 'user/login'.
 * @param params
 *   Array of query string parameters.
 */
function setRedirect(path, params) {
	var gotourl = buildUrl(path, params);
	alert('About to redirect to ' + gotourl);
	window.location = gotourl;
}

/**
 * Builds URL with path and params
 */
function buildUrl(path, params) {
	var query = new Array();
	var name,i;
	if (params) {
		i = 0;
		for (name in params) {
			//alert('name=' + name + ' = ' + params[name]);
			query[i++] = name + '=' + encodeURI(params[name]);
		}
		path += '?' + query.join('&');
	}
	return path;
}
