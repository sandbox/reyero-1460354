<?php
/**
 * @file
 * Malo page callbacks
 */

/**
 * View capture
 */
function malo_page_capture_view($capture) {
  $rows[] = array(t('Id'), $capture->mcid);
  $rows[] = array(t('User'), theme('username', array('account' => user_load($capture->uid))));
  $node = node_load($capture->nid);
  $rows[] = array(t('Blog'), l($node->title, 'node/' . $node->nid));
  $rows[] = array(t('Title'), check_plain($capture->title));
  $rows[] = array(t('Content'), filter_xss_admin($capture->content));
  $rows[] = array(t('Time'), format_date($capture->timestamp));
  $rows[] = array(t('URL'), check_plain($capture->location));
  $rows[] = array(t('Host'), check_plain($capture->hostname));
  $rows[] = array(t('Referer'), check_plain($capture->referer));

  $build['malo_capture'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
  );
  return $build;
}

/**
 * View captured data
 */
function malo_page_view_captured($node) {
  $list = entity_load('malo_capture', FALSE, array('nid' => $node->nid));
  $header = array(t('User'), t('Title'), t('Data'));
  $rows = array();
  foreach ($list as $capture) {
    $rows[] = array(
      theme('username', array('account' => user_load($capture->uid))),
      check_plain($capture->title),
      filter_xss_admin($capture->content),
    );
  }
  $build['captured'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('No captured data'),
  );
  return $build;
}

/**
 * View node raw content
 */
function malo_page_format_content($node) {
  drupal_set_title($node->title);
  return node_view($node, 'content');
}

/**
 * View user secrets
 */
function malo_page_user_view() {
  $account = user_load($GLOBALS['user']->uid);
  if (_malo_user_access($account)) {
    $build['secrets'] = malo_user_view_secrets($account);
  }
  $build['session'] = malo_user_view_session();
  return $build;
}

/**
 * View user cookies.
 *
 * Note the cookie name is vulnerable to XSS. !!!
 */
function malo_user_view_session() {
  // Prepare cookies
  if (!empty($_COOKIE)) {
    foreach ($_COOKIE as $name => $value) {
      $cookies[] = array($name, check_plain($value));
    }
    $build['cookies'] = array(
      '#type' => 'fieldset',
      '#title' => t('Cookies'),
    );
    $build['cookies']['data'] = array(
      '#theme' => 'table',
      '#rows' => $cookies,
    );
  }
  // Session data
  $build['session'] = array(
    '#type' => 'fieldset',
    '#title' => t('Session'),
  );
  $build['session']['name'] = array(
    '#title' => t('Session name'),
    '#type' => 'item',
    '#markup' => session_name(),
  );
  if (drupal_session_started()) {
    $build['session']['id'] = array(
      '#title' => t('Session Id'),
      '#type' => 'item',
      '#markup' => session_id(),
    );
  }
  $domain = $GLOBALS['cookie_domain'];
  $session_domain = ini_get('session.cookie_domain');
  if ($session_domain && $domain != $session_domain) {
    $domain .= ' (' . $session_domain . ')';
  }
  $build['session']['domain'] = array(
    '#title' => t('Cookie domain'),
    '#type' => 'item',
    '#markup' => $domain,
  );
  return $build;
}

/**
 * Edit user secrets.
 *
 * Unsecured form vulnerable to CSRF.
 */
function malo_page_user_secrets() {
  $account = user_load($GLOBALS['user']->uid);
  // Check form submission
  if (($request = malo_get_request()) && malo_set_user_secrets($account, $request)) {
    drupal_set_message(t('Your secrets have been updated.'));
    // If coming from a different page, store it
    if (malo_get_referer(TRUE)) {
      $capture = array(
        'type' => 'save',
        'uid' => $account->uid,
      	'title' => t('Updated user secrets'),
      );
      malo_page_store_capture($capture);
    }
    drupal_goto('malo/secrets');
  }
  else {
    // Add insecure form
    $secreto = malo_user_field_value($account, 'secreto');
    $numero = malo_user_field_value($account, 'numero');
    $include = drupal_get_path('module', 'malo') . '/malo.form.inc';
    return malo_get_include_contents($include, $secreto, $numero);
  }
}

/**
 * Reset user secrets.
 *
 * Unsecured form vulnerable to CSRF.
 */
function malo_page_user_reset() {
  $account = user_load($GLOBALS['user']->uid);
  $data = array('secreto' => '', 'numero' => 0);
  malo_set_user_secrets($account, $data);
  drupal_set_message(t('Your secrets have been reset.'));
  // If coming from a different page than 'malo', capture data.
  if (malo_get_referer(TRUE)) {
    $capture = array(
      'type' => 'reset',
      'uid' => $account->uid,
      'title' => t('Reset user secrets'),
    );
    malo_page_store_capture($capture);
  }
  drupal_goto('malo');
}

/**
 * Store capture
 */
function malo_page_store_capture($capture) {
  $request = malo_get_request();
  if ($node = malo_get_referer_node()) {
    malo_save_capture_node($node, $capture, $request);
  }
  else {
    // We don't know the user who captured the data
    $capture['owner'] = 0;
    malo_save_capture($capture, $request);
  }
}

/**
 * Get include contents
 */
function malo_get_include_contents($filename, $secreto, $numero) {
  if (is_file($filename)) {
    ob_start();
    include $filename;
    return ob_get_clean();
  }
  return false;
}


/**
 * View or set cookies
 */
function malo_page_user_cookies() {
  // 'Fix' secure session parameters
  if (!drupal_session_started()) {
    // Start session for anonymous users.
    $_SESSION['anonymous-tracker'] = time();
    drupal_session_start();
  }
  $build['set'] = drupal_get_form('malo_cookies_form');
  $build['view'] = malo_user_view_session();
  return $build;
}

/**
 * Form to set cookies.
 */
function malo_cookies_form($form, &$form_state, $count = 1) {
  $form['cookies']= array(
  	'#tree' => TRUE,
    '#type' => 'fieldset',
  );
  $form['cookies']['name'] = array(
    '#title' => t('Name'),
    '#type' => 'textfield',
  );
  $form['cookies']['value'] = array(
    '#title' => t('Value'),
    '#type' => 'textfield',
  );
  $form['set'] = array('#type' => 'submit', '#value' => t('Set cookies'));
  if (drupal_session_started()) {
    $form['reset'] = array('#type' => 'submit', '#value' => t('Reset session'));
  }
  return $form;
}

/**
 * Set cookies and return
 */
function malo_cookies_form_submit($form, &$form_state) {
  global $cookie_domain;
  if ($form_state['values']['op'] == t('Set cookies')) {
    $cookies = $form_state['values']['cookies'];
    if (!empty($cookies['name'])) {
      setcookie($cookies['name'], $cookies['value'], 0, '/', $cookie_domain);
      //$_COOKIE[$data['name']] = $data['value'];
      drupal_save_session(FALSE);
    }
  }
  elseif ($form_state['values']['op'] == t('Reset session')) {
    //_drupal_session_destroy(session_id());
    session_destroy();
  }
}

/**
 * Insecure user login
 *
 * Allows user login without changing the session id.
 */
function malo_page_insecure_login() {
  global $user;
  // Start session always for anonymous
  if (!drupal_session_started()) {
    // Start session for anonymous users.
    $_SESSION['anonymous-tracker'] = time();
    drupal_session_start();
  }
  if ($user->uid) {
    menu_set_active_item('user/' . $user->uid);
    return menu_execute_active_handler(NULL, FALSE);
  }
  else {
    $build['form'] = drupal_get_form('malo_user_login_form');
    $build['session'] = malo_user_view_session();
    return $build;
  }
}

/**
 * Insecure login form.
 */
function malo_user_login_form($form, &$form_state) {
  $form = user_login($form, $form_state);
  /*
  if (drupal_session_started()) {
    $form['session'] = array(
      '#type' => 'item',
      '#title' => t('Session Id'),
      '#markup' => session_id(),
    );
  }
  */
  // Add final validate callback
  $form['#validate'][] = 'malo_user_login_validate';
  return $form;
}

/**
 * Final validation step, fix session.
 */
function malo_user_login_validate($form, &$form_state) {
  if (!empty($form_state['uid'])) {

  }
}

/**
 * Insecure login submission
 *
 * Submit handler for the login form. Load $user object and perform standard login
 * tasks. The user is then redirected to the My Account page. Setting the
 * destination in the query string overrides the redirect.
 */
function malo_user_login_form_submit($form, &$form_state) {
  global $user;
  $user = user_load($form_state['uid']);
  $form_state['redirect'] = 'user/' . $user->uid;
  // Call instead of user_login_finalize().
  malo_user_login_finalize($form_state);
}

/**
 * Finalize the login process. Must be called when logging in a user.
 *
 * The function records a watchdog message about the new session, saves the
 * login timestamp, calls hook_user op 'login' and generates a new session. *
 */
function malo_user_login_finalize(&$edit = array()) {
  global $user;
  watchdog('malo', 'Insecure session opened for %name.', array('%name' => $user->name));
  // Update the user table timestamp noting user has logged in.
  // This is also used to invalidate one-time login links.
  $user->login = REQUEST_TIME;
  db_update('users')
    ->fields(array('login' => $user->login))
    ->condition('uid', $user->uid)
    ->execute();

  // Regenerate the session ID to prevent against session fixation attacks.
  // This is called before hook_user in case one of those functions fails
  // or incorrectly does a redirect which would leave the old session in place.
  //drupal_session_regenerate();

  // In order to update user for session, we run our own session update.
  global $user, $is_https;
  $session_id = session_id();
  $fields = array('sid' => $session_id);

  // Warn the user
  drupal_set_message(t('Insecure Welcome to %site, session is %session', array('%site' => variable_get('site_name', 'Drupal'), '%session' => $session_id)));
  user_module_invoke('login', $edit, $user);
}

/**
 * Menu callback; logs the current user out, and redirects to the home page.
 */
function malo_page_user_logout() {
  global $user;

  watchdog('user', 'Session closed for %name.', array('%name' => $user->name));

  module_invoke_all('user_logout', $user);

  // Destroy the current session, and reset $user to the anonymous user.
  // session_destroy();

  // SECURITY!!! Instead, reset to anoymous user and keep the session.
  $user = drupal_anonymous_user();

  drupal_goto('user/malo');
}

/**
 * Majo injection form
 */
function malo_page_malo_malo() {
  $build['form'] = drupal_get_form('malo_page_malo_form');
  $build['session'] = malo_user_view_session();
  return $build;
}

/**
 * Injection form
 */
function malo_page_malo_form($form, &$form_state) {
  if (!empty($form_state['values'])) {
    $url = drupal_get_path('module', 'malo') . '/malo.php';
    $options['query']['name'] = $form_state['values']['name'];
    $options['absolute'] = TRUE;
    $options['attributes']['target'] = '_blank';
    $form['attack'] = array(
      '#type' => 'item',
      '#title' => t('Attack link'),
      '#markup' => l(url($url, $options), $url, $options),
    );
  }
  $form['name'] = array(
    '#type' => 'textarea',
  	'#title' => t('Name'),
  );
  $form['build'] = array(
    '#type' => 'submit',
    '#value' => t('Build link'),
  );
  return $form;
}

/**
 * Injection form
 */
function malo_page_malo_form_submit($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
}